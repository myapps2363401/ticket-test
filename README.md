# Описание

## Как выбрать файл
В файле Settings.json указать путь. Пример:
```
 "Path": "D:\\promit\\Receipt_006.txt"
```
	
## Как запускать тесты
В CLI ввести:
```
 dotnet test
```