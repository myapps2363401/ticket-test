﻿using Core.Config.ModelsConfig;
using Microsoft.Extensions.Configuration;

namespace TicketTest.Config
{
    public static class ConfigProvider
    {
        private static IConfigurationRoot GetSettings
        {
            get
            {
                return new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile($"Settings.json", true, false)
                   .Build();
            }
        }

        public static Settings? Settings => GetSettings.Get<Settings>();

        public static T? GetGenericVal<T>(string key)
        {
            return GetSettings.GetValue<T>(key);
        }
    }
}
