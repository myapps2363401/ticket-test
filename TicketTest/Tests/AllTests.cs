﻿using System.Text.RegularExpressions;
using TicketTest.Config;

namespace TicketTest.Tests
{
    [TestFixture]
    public class AllTests
    {
        private string[] ticketInfo;

        [OneTimeSetUp]
        public void Setup()
        {
            string path = ConfigProvider.Settings.Path;
            ticketInfo = File.ReadAllLines(path);
        }

        [Test, Description("Проверка даты поездки")]
        public void TestExtractDate()
        {
            string line = ticketInfo[2];
            string datePattern = @"\d{1}\s\d{1}\s\.\s\d{1}\s\d{1}\s\.\s\d{1}\s\d{1}\s\d{1}\s\d{1}";
            Match dateMatch = Regex.Match(line, datePattern);
            Assert.IsTrue(dateMatch.Success, "Дата поездки не найдена");
            Console.WriteLine("Дата поездки: " + dateMatch.Value);
        }

        [Test, Description("Проверка станции отправления")]
        public void TestExtractStationDeparture()
        {
            string line = ticketInfo[3];
            string stationPattern = @"от\s+\w+[-. ]\s?(\d+|\w+)\s?\w+";
            Match stationMatch = Regex.Match(line, stationPattern);
            Assert.IsTrue(stationMatch.Success, "Станция отправления не найдена");
            Console.WriteLine("Станция отправления: " + stationMatch.Value);
        }

        [Test, Description("Проверка станции назначения")]
        public void TestExtractStationDestination()
        {
            string line = ticketInfo[4];
            string stationPattern = @"до\s+\w+[-. ]\s?(\d+|\w+)\s?\w+";
            Match stationMatch = Regex.Match(line, stationPattern);
            Assert.IsTrue(stationMatch.Success, "Станция назначения не найдена");
            Console.WriteLine("Станция назначения: " + stationMatch.Value);
        }

        [Test, Description("Проверка номера билета")]
        public void TestExtractTicketNumber()
        {
            string line = ticketInfo[5];
            string ticketPattern = @"Билет N: \d+";
            Match ticketMatch = Regex.Match(line, ticketPattern);
            Assert.IsTrue(ticketMatch.Success, "Номер билета не найден");
            Console.WriteLine("Номер билета: " + ticketMatch.Value.Remove(0, 9));
        }

        [Test, Description("Проверка системного номера")]
        public void TestExtractSystemNumber()
        {
            string line = ticketInfo[5];
            string systemNumberPattern = @"Сист.N:\s+\d+";
            Match systemNumberMatch = Regex.Match(line, systemNumberPattern);
            Assert.IsTrue(systemNumberMatch.Success, "Системный номер не найден");
            Console.WriteLine("Системный номер: " + systemNumberMatch.Value.Remove(0, 8));
        }

        [Test, Description("Проверка перевозки")]
        public void TestExtractTransport()
        {
            string line = ticketInfo[6];
            string transportPattern = @"Перевозка .+ \d+";
            Match transportMatch = Regex.Match(line, transportPattern);
            Assert.IsTrue(transportMatch.Success, "Информация о перевозке не найдена");
            Console.WriteLine("Перевозка: " + transportMatch.Value);
        }

        [Test, Description("Проверка стоимости по тарифу")]
        public void TestExtractTariffCost()
        {
            string line = ticketInfo[7];
            string tariffPattern = @"Стоимость по тарифу:\s+=\d+\.\d+";
            Match tariffMatch = Regex.Match(line, tariffPattern);
            Assert.IsTrue(tariffMatch.Success, "Стоимость по тарифу не найдена");
            Console.WriteLine("Стоимость по тарифу: " + tariffMatch.Value.Remove(0, 33));
        }

        [Test, Description("Проверка итого")]
        public void TestExtractTotal()
        {
            string line = ticketInfo[8];
            string totalPattern = @"ИТОГ: \d+,\d+";
            Match totalMatch = Regex.Match(line, totalPattern);
            Assert.IsTrue(totalMatch.Success, "Итоговая сумма не найдена");
            Console.WriteLine("Итог: " + totalMatch.Value.Remove(0, 6));
        }
    }
}
